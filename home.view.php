<!DOCTYPE html>
<div lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/myStyle.css">
    <title>Hussein Kassem's Profile</title>
</head>
<div>
<div class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"> </span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="#">Top</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Work experience</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-2 bg-white">
        </div>
        <div class="col-8 bg-white">
            <h1 class="h1">CV Hussein Kassem </h1>
        </div>
        <div class="col -2 bg-white">
        </div>
    </div>

<!-- My personel-->
    <div class="row mt-2">
        <div class="col-2 bg-white">
        </div>
        <div class="col-8 bg-transparent">
                        <div class="card">
                <img src="Img/mijn footo.png" class="card-img-top w-50 img-thumbnail mx-auto m-2 img-fluid" alt="mijn footo">
                <div class="card-body">
                    <h5 class="card-title">Mijn persoon </h5>
                    <p class="card-text">
                        Sinds ik aan de universiteit ben afgestudeerd, werk Ik. ik heb in
                        verschillende vakgebieden kunnen werken en tijdens mijn professionele leven met geduld en
                        grote inspanning eventuele problemen kunnen oplossen. Ik ga altijd nieuwe uitdagingen aan om
                        meer in mijn leven te bereiken. Werk is een van de belangrijke aspecten van het leven.
                    </p>
                </div>
               <a href="https://www.google.nl" target="_blank" class="btn btn-primary">Go to my website</a>

            </div>
        </div>
        <div class="col -2 bg-white">
        </div>
 </div>

<!-- End My personel-->
<!-- My job experience-->
    <div class="row mt-2">
        <div class="col-2 bg-white">
        </div>
        <div class="col-8 bg-transparent">
            <h3 class="bg-transparent mt-auto">Mijn Study</h3>
            <?php foreach ($profile as $study) :?>
            <div class="card">
            <img src="<?= $study->logo?>" class="card-img-top w-25 img-thumbnail mx-auto m-2 p-2 img-fluid" alt="mijn werkervaring">
                <div class="card-body">
                    <h5 class="card-title"><?= $study->organization?> </h5>
                    <p class="card-text"><?= $study->description?> </p>
                </div>
                <div class="card-footer"> <?= $study->degree ? 'Geslaagd':'Nog niet behaald'?> <?= date('d/m/Y',strtotime($study->degredate))?>
                </div>
            </div>
            <?php endforeach;?>
        </div>
            <div class="col -2 bg-white"> </div>

        </div>

        <div class="row mt-2">
            <div class="col-2 bg-white"> </div>
            <div class="col-8 bg-transparent">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        <h2 class=" bg-transparent"> Mijn Hobby's</h2>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="Img/chess.png" class="d-block w-100" alt="Chess">
                            <div class="card">
                                <h8 class="h8 bg-transparent"> Chess playing </h8>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="Img/volleyball.png" class="d-block w-100" alt="Volleyball">
                            <div class="card">
                                <h8 class="h8 bg-transparent">  Volleyball playing </h8>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="Img/vissen.png" class="d-block w-100" alt="Vissen sport">
                            <div class="card">
                                <h8 class="h8 bg-transparent"> Fishing </h8>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>

            <div class="col-2 bg-white"></div>
        </div>
    </div>

<!-- End My job experience-->
<!-- My Hobby's-->
<!-- End My Hobby's-->
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
        crossorigin="anonymous"></script>
</div>
